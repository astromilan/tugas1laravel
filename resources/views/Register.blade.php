<!-- Halaman FORM form.html -->
<!DOCTYPE html>
<html>
<head>
<title>Tugas 1</title>
</head>
<body>

<h1>Buat Account Baru!</h1>

<h4>Sign Up Form</h4>

<form action="welcome.html">
    <label for="first">First name:</label><br><br>
    <input type="text" placeholder="First Name" value="firstname" id="first"><br><br>
    <label for="last">Last name:</label><br><br>
    <input type="text" placeholder="Last Name" value="lastname" id="last"><br><br>
    <label>Gender: </label><br><br>
    <input type="radio" name="gender" value="male">male<br>
    <input type="radio" name="gender" value="female">female<br>
    <input type="radio" name="gender" value="Other">other<br><br>
    <label>Nationality:</label>
    <br>
    <select>
        <option value="Indonesian">Indonesian</option>  
        <option value="Indonesian">Singapura</option>
        <option value="Indonesian">Malaysia</option>
        <option value="Indonesian">Australia</option>
    </select>
    <br><br>
    <label>Language Spoken:</label>
    <br><br>
    <input type="checkbox" name="language" value="Bahasa">Bahasa Indonesia<br>
    <input type="checkbox" name="language" value="English">English<br>
    <input type="checkbox" name="language" value="Others">Others<br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea cols="30" rows="7" id="bio">   </textarea>
    <br>
    <input type="submit" name="submit" value="Sign Up">
</form>
</body>
</html>
